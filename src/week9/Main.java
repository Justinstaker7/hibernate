package week9;

//imports
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import java.util.Iterator;
import java.util.List;
public class Main {

    //want to point out that I had a lot of help with this Java main class from Carlos who is in my group

    private static SessionFactory factory;
    public static void main(String[] args)  {

        //Registry object
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();

        try { // code to build a session with MySQL
            factory = new MetadataSources(registry)
                    .buildMetadata().buildSessionFactory();
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            //videogames to be entered into videogames table
            VideoGames videoGames = new VideoGames();
            videoGames.setId(1);
            videoGames.setTitle("FIFA 20");
            videoGames.setDeveloper("EA Games");
            videoGames.setRelease_Date("2019");
            videoGames.setConsole("Playstation");

            session.save(videoGames);

            transaction.commit();

            listGames();
            session.close();
            factory.close();

        } catch (Exception e) { //destroy connection if no connection is made
            System.out.println(e.getMessage());
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }

    }

        //open session
        public static void listGames(){
            Session session = factory.openSession();
            Transaction transaction = null;

            try {
                transaction = session.beginTransaction();
                List objects = session.createQuery("from VideoGames ").list();
                for (Iterator iterator = objects.iterator(); iterator.hasNext();){
                    VideoGames videoGames = (VideoGames) iterator.next();
                    System.out.println("");
                    System.out.println("id: " + videoGames.getId());
                    System.out.println("Title: " + videoGames.getTitle());
                    System.out.println("Developer: " + videoGames.getDeveloper());
                    System.out.println("Release_Date: " + videoGames.getRelease_Date());
                }
                transaction.commit();
            }catch (HibernateException e){
                if(transaction != null) transaction.rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }
    }



