package week9;

import javax.persistence.*;

//establish values/connection to table in database
@Entity
@Table(name = "videogames")
public class VideoGames {

    //columns that will have data input to them
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "Title")
    private String Title;

    @Column(name = "Developer")
    private String Developer;

    @Column(name = "Release_Date")
    private String Release_Date;

    @Column(name = "Console")
    private String Console;

    /** getters and setters for each column (method) **/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }


    public String getDeveloper() {
        return Developer;
    }

    public void setDeveloper(String Developer) {
        this.Developer = Developer;
    }

    public String getRelease_Date() {
        return Release_Date;
    }

    public void setRelease_Date(String Release_Date) {
        this.Release_Date = Release_Date;
    }

    public String getConsole(){
        return Console;
    }

    public void setConsole(String Console){
        this.Console = Console;
    }

    //to string output
    public String toString() {
        return Integer.toString(id) + " " + Title + " " + Developer + " " + Release_Date + " " + Console;
    }
}
